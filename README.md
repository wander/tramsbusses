## TramsBusses

Retrieves live OV data from NDOVLoket.

## Dependencies
- Utils
- Broker (seperate .exe)

### Usage:

1. Start the broker first. This is a seperate exe. (See OVAPI GIT project).
2. Sweep the TramsBusses prefab in the scene.
3. Select the terrain prefab and press 'Sync Coords' or manaully adjust the WGS84 (latitude longitude coordinates).
4. Select the TramsBusses prefab and press 'Restart' (alsoRunInEditor must be ticked to have it work in editor mode without playing.)


![](./sample0.gif)