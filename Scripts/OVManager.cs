using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using WanderOV;

namespace Wander
{
    [Serializable]
    public struct OVPrefab
    {
        public string type;
        public GameObject prefab;
    }

    public class OVGameObject
    {
        public GameObject vehicle;
        public OVVehicleBehaviour behaviour;
    }

    [ExecuteAlways()]
    public class OVManager : MonoBehaviour
    {
#pragma warning disable CS0414
        [ReadOnly] public Vector2 originRD;
        [ReadOnly] public GameObject ovRoot;
#pragma warning restore CS0414

        public bool runAlsoInEditor = true;
        public string connAddress   = "127.0.0.1";
        public int connPort         = 9999;
        public float wgs84Lat       = 51.86567f;
        public float wgs84Lon       = 5.125492f;
        public float catchRadius    = 3000;
        public long requestOvDelayMs = 1000;
        public List<OVPrefab> ovPrefabs;

        bool run = false;
        OVClient client;
        Dictionary<OVVehicle, GameObject> unityVehicles;
        List<OVVehicle> pendingRemoves = new List<OVVehicle>();

        public void SetGPSCoord( Dictionary<string, string> boundaryData )
        {
            wgs84Lat = (float)double.Parse( boundaryData["gpsx"] );
            wgs84Lon = (float)double.Parse( boundaryData["gpsy"] );
            RDUtils.GPS2RD( wgs84Lat, wgs84Lon, out var originRdX, out var originRdY );
            originRD = new Vector2( (float)originRdX, (float)originRdY );

#if UNITY_EDITOR
            EditorUtility.SetDirty( this );
#endif
        }

        void Start()
        {
            if ((runAlsoInEditor && Application.isEditor) || Application.isPlaying)
            {
                Restart();
            }
        }

        void Restart()
        {
            EnforceOVAPIIsRunning();
            RDUtils.GPS2RD( wgs84Lat, wgs84Lon, out var originRdX, out var originRdY );
            originRD = new Vector2( (float)originRdX, (float)originRdY );
            client?.Stop();
            ovRoot.Destroy();
            client = new OVClient( originRD, connAddress, connPort, catchRadius, requestOvDelayMs );
            unityVehicles = new();
            client.Start();
            run = true;
        }

        void EnforceOVAPIIsRunning()
        {
            bool isRunning = Process.GetProcessesByName("OVAPI").Any();
            if (!isRunning)
            {
                var executableDirectory = Application.isEditor ? Application.dataPath.Combine( "..\\" ) : AppContext.BaseDirectory;
                var path = Application.dataPath.Combine( executableDirectory, "OVAPI_bin", "OVAPI.exe" );
                Process.Start( path );
            }
        }

        void Update()
        {
            if (!runAlsoInEditor && !Application.isPlaying)
            {
                client?.Stop();
                ovRoot?.Destroy();
                ovRoot = null;
                client = null;
                unityVehicles?.Clear();
                unityVehicles=null;
                run = false;
            }

            // Do NOT use 'enabled'. Because if not playing in editor, it is set to 'false' and therefore serialized in game as 'false' too, which is undesired.
            if (run)
            {
                UpdateGameObjects();
            }
        }

        void UpdateGameObjects()
        {
            if (unityVehicles == null) // Happens after recompiling unity in editor.
            {
                Restart(); 
                return;
            }

            if (ovRoot == null)
            {
                ovRoot = new GameObject( "Openbaar Vervoer" );
                var offCanvasGo = new GameObject("OffCanvas");
                offCanvasGo.transform.parent=ovRoot.transform;
            }

            var ovVehicles = client.GetVehicles();
            for (int i = 0;i < ovVehicles.Count;i++)
            {
                var ovVehicle = ovVehicles[i];
                var ip = ovVehicle.InterpolatedPositionWS( originRD );
                //var ip = ovVehicle.ActualPositionWS(originRD);

                OVVehicleBehaviour ovBehaviour = null;
                if (!unityVehicles.TryGetValue( ovVehicle, out var ovGo ))
                {
                    OVPrefab ovPrefab = ovVehicle.route != null ? ovPrefabs.Find( ovprfb => ovVehicle.route.transportType.Contains(ovprfb.type) ) : default;
                    if (ovPrefab.prefab == null)
                    {
                        ovPrefab = ovPrefabs.Find( ovprfb => ovprfb.type == "unknown" );
                    }
                    if (ovPrefab.prefab == null)
                    {
                        continue;
                    }
                    ovGo = Instantiate( ovPrefab.prefab, ip.posWS, Quaternion.identity );
                    ovBehaviour = ovGo.GetComponent<OVVehicleBehaviour>();
                    ovBehaviour.ovManager = this;
                    ovBehaviour.ovVehicle = ovVehicle;
                    ovBehaviour.routeRevision = -1;
                    unityVehicles.Add( ovVehicle, ovGo );
                }
                else if (ovGo !=null)
                {
                    ovBehaviour = ovGo.GetComponent<OVVehicleBehaviour>();
                }
                else continue;

                bool onCanvas = ip.posWS.RaycastDown( 200, 500, LayerMask.GetMask( "Default" ), out var hit );
                if (ovVehicle.RouteRevision != ovBehaviour.routeRevision)
                {
                    ovBehaviour.UpdateNameAndLineNumber( onCanvas );
                    ovGo.transform.SetParent( onCanvas ? ovRoot.transform : ovRoot.transform.GetChild( 0 )/*Child_0 is off canvas objects.*/ );
                }

                ovGo.transform.position = onCanvas ? hit.point : ip.posWS;
                ovGo.transform.rotation = Quaternion.Euler( 0, ip.angDeg, 0 );
                ovBehaviour.updateRevision = Time.frameCount;
            }
            ovRoot.name = $"OV ({unityVehicles.Count})";

            // If is not updated, remove vehicle. All vehicles always update.
            foreach ( var kvp  in unityVehicles )
            {
                var vehicle = kvp.Value;
                var behaviour = vehicle.GetComponent<OVVehicleBehaviour>();
                if (behaviour.updateRevision != Time.frameCount)  
                {
                    vehicle.Destroy();
                    pendingRemoves.Add( kvp.Key );
                }
            }

            pendingRemoves.ForEach( pr => unityVehicles.Remove( pr ) );
            pendingRemoves.Clear();
        }

        private void OnDestroy()
        {
            client?.Stop();
            client=null;
        }

#if UNITY_EDITOR
        [CustomEditor( typeof( OVManager ) )]
        [InitializeOnLoad]
        public class TramBusEditor : Editor
        {
            public override void OnInspectorGUI()
            {
                OVManager trambus = (OVManager)target;

                GUILayout.BeginHorizontal();
                {
                    if (GUILayout.Button( "Restart" ))
                    {
                        trambus.Restart();
                    }
                    if (GUILayout.Button( "Stop" ))
                    {
                        trambus.run = false;
                        trambus.ovRoot.Destroy();
                        trambus.ovRoot = null;
                        trambus.client?.Stop();
                        trambus.client=null;
                    }
                }
                GUILayout.EndHorizontal();

                DrawDefaultInspector();
            }
        }

#endif
    }
}