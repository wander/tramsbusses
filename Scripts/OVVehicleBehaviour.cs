using System.Collections;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using WanderOV;

namespace Wander
{
    [ExecuteAlways()]
    public class OVVehicleBehaviour : MonoBehaviour
    {
        [ReadOnly] public Camera cam;
        [ReadOnly] public OVManager ovManager;
        [ReadOnly] public OVVehicle ovVehicle;
        [ReadOnly] public int routeRevision;
        [ReadOnly] public int updateRevision;
        [ReadOnly] public float lastPdx;
        [ReadOnly] public float lastPdy;
        [ReadOnly] public long lastTime;

        public TMPro.TextMeshPro number;
        public TMPro.TextMeshPro directionInfo;

        void Start()
        {
            StartCoroutine( UpdateDestination() );
        }

        IEnumerator UpdateDestination()
        {
            while (true)
            {
                if (ovVehicle != null)
                {
                    var route = ovVehicle.Route;
                    if (route != null)
                    {
                        float closest = float.MaxValue;
                        int closestStop = -1;
                        for (int i = 0;i < route.NumStops;i++)
                        {
                            var stopcoord = route.StopCoord( i ) - ovManager.originRD;
                            var deltaDist = Vector3.Distance( new Vector3( stopcoord.x, transform.position.y, stopcoord.y ),  transform.position );
                            if (deltaDist < closest)
                            {
                                closest = deltaDist;
                                closestStop = i;
                            }
                        }
                        if (closestStop >= 0)
                        {
                            directionInfo.text = route.StopDestination( closestStop );
                        }
                    }
                }
                yield return new WaitForSeconds( 3 );
            }
        }

        //private void Update()
        //{
        //    if (cam == null)
        //        return;
        //    var lookDir = cam.transform.forward;
        //    lookDir.y=0;
        //    lookDir.Normalize();
        //    number.transform.parent.rotation = Quaternion.LookRotation( -lookDir );
        //}

        public void UpdateNameAndLineNumber( bool onCanvas )
        {
            routeRevision = ovVehicle.RouteRevision;
            number.text   = ovVehicle.LineDisplayNumber; // Number sign on object.
            // Owner, vehiclenummer, journeyNumb, displayNumb
            name = string.Join( ':', ovVehicle.owner, ovVehicle.route != null ? ovVehicle.route.transportType : "unknown", ovVehicle.vehicleNumb, ovVehicle.journeyNumb, ovVehicle.lineDisplayNumb );
        }

        private void OnDrawGizmosSelected()
        {
            if (ovVehicle == null)
                return;

            lastPdx = ovVehicle.predictedRD.x;
            lastPdy = ovVehicle.predictedRD.y;
            if (ovVehicle.sampledPositions3D != null && ovVehicle.sampledPositions3D.Count != 0)
                lastTime = ovVehicle.sampledPositions3D.Last().time;

            var route = ovVehicle.route;
            if (route == null)
                return;

            Gizmos.color = Color.green;
            var points = route.routePoints;
            var of = ovManager.originRD;
            for (int i = 0;i < points.Count;i++)
            {
                var point = points[i];
                var pos = point - of;
                float yy = 0;
                if (new Vector3( pos.x, 0, pos.y ).RaycastDown( 200, 500, LayerMask.GetMask( "Default" ), out var hit2 ))
                    yy = hit2.point.y+5;
                Gizmos.DrawWireCube( new Vector3( pos.x, yy, pos.y ), Vector3.one*0.3f );
                if (i!=points.Count-1)
                {
                    Gizmos.DrawLine( new Vector3( pos.x, yy, pos.y ), new Vector3( points[i+1].x-of.x, yy, points[i+1].y-of.y ) );
                }
            }

            Gizmos.color = Color.red;
            var actualPos = ovVehicle.ActualPositionWS(ovManager.originRD);
            float y = 0;
            if (actualPos.RaycastDown( 200, 500, LayerMask.GetMask( "Default" ), out var hit ))
                y = hit.point.y+5;
            Gizmos.DrawCube( actualPos + new Vector3(0, y, 0) , Vector3.one*10.5f );
            Gizmos.DrawLine( actualPos, actualPos+new Vector3( 0, 100, 0 ) );

            Gizmos.color = Color.blue;
            var predictedPos = ovVehicle.PredictedPositionWS(ovManager.originRD);
            y = 0;
            if (predictedPos.RaycastDown( 200, 500, LayerMask.GetMask( "Default" ), out var hit3 ))
                y = hit3.point.y+5;
            Gizmos.DrawWireCube( predictedPos + new Vector3( 0, y, 0 ), Vector3.one*10.5f );
            Gizmos.DrawLine( predictedPos, predictedPos+new Vector3( 0, 100, 0 ) );

            if ( Random.Range(0, 60)==0) 
                print( "Speed: " + ovVehicle.speedMtrps*3.6f + " km/h" );
        }

    }
}