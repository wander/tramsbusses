﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;

namespace WanderOV
{
    public enum VehicleInfoType
    {
        MustTeleport,
        Owner,
        LineDisplayNumber,
        LinePrivateNumber,
        JourneyNumber,
        VehicleNumber,
        Punctuality,
        Rdx,
        Rdy,
        PredictedRdx,
        PredictedRdy,
        AngleDeg,
        SpeedMtrps,
        Unknown
    }

    public class OVRoute
    {
        internal string lineName;
        internal string transportType;
        internal string lineDisplayNumb;
        internal string linePrivateNumb;
        internal List<(string name, Vector2 pos, string dest)> stops;
        internal List<Vector2> routePoints;

        public string LineName => lineName;
        public string TransportType => transportType;
        public string LineDisplayNumber => lineDisplayNumb;
        public int NumStops => stops.Count;
        public Vector2 StopCoord( int idx ) => stops[idx].pos;
        public string StopName(int idx) => stops[idx].name;
        public string StopDestination( int idx ) => stops[idx].dest;
        public IList<Vector2> RoutePoints => routePoints.AsReadOnly();
    }

    public class OVVehicle
    {
        internal OVClient client;
        internal string owner; // CXX, QBUZZ, ARR, EBS, etc.
        internal string lineDisplayNumb;
        internal string vehicleNumb;
        internal string linePrivateNumb;
        internal string journeyNumb;
        internal int punctuality = 0;      // Delay or Ahead time in seconds.
        internal Vector2 positionRD;       // Last known position from GPS
        internal Vector2 predictedRD;      // Predicted position from last GPS forwarded in time.
        internal float angleDeg;
        internal float speedMtrps;          // Speed mtr/sec
        internal List<(Vector2 pos, float ang, long time)> sampledPositions3D;  // All forward predicted positions are added in a buffer with a timestamp.
        internal OVRoute route;
        internal string key;
        internal int routeRevision;
        internal bool netUpdated;

        public int RouteRevision => routeRevision; 
        public string Owner => owner;
        public string LineDisplayNumber => lineDisplayNumb;
        public string VehicleNumber => vehicleNumb;
        public string JourneyNumber => journeyNumb;
        public int Punctuality => punctuality;
        public Vector2 PositionRD => positionRD;
        public Vector2 PredirectedPos => predictedRD;
        public float AngleDeg => angleDeg;
        public OVRoute Route => route;
        public string Key => key;

        internal void CleanPredictedSamples()
        {
            lock(this)
            {
                if (sampledPositions3D==null)
                    return;
                sampledPositions3D.Clear();
            }
        }

        internal void AddPredictedPositionRD()
        {
            lock (this)
            {
                if (sampledPositions3D==null)
                    sampledPositions3D = new();

                // Cleanup outdated samples
                var timeNow = client.ElapsedMilliseconds;
                long timeLongAgo = timeNow - client.RequestDelay*4;
                for (int i = 0;i < sampledPositions3D.Count;i++)
                {
                    if (sampledPositions3D[i].time < timeLongAgo)
                    {
                        sampledPositions3D.RemoveAt( i );
                        i--;
                    }
                    else break;
                }

                sampledPositions3D.Add( (predictedRD, angleDeg, client.ElapsedMilliseconds) );
            }
        }

        internal (Vector2 pos, float ang) InterpolatedPositionRD()
        {
            lock (this)
            {
                if (sampledPositions3D==null || sampledPositions3D.Count==0)
                    return (positionRD, angleDeg);

                if (sampledPositions3D.Count==1)
                    return (sampledPositions3D[0].pos, sampledPositions3D[0].ang);

                var timeNow = client.ElapsedMilliseconds;
                var lookupTime = timeNow - client.RequestDelay*1.35f;

                for (int i = 0;i < sampledPositions3D.Count-1;i++)
                {
                    if (lookupTime >= sampledPositions3D[i].time && lookupTime <= sampledPositions3D[i+1].time)
                    {
                        float alpha = (lookupTime-sampledPositions3D[i].time) / (sampledPositions3D[i+1].time - sampledPositions3D[i].time);
                        var pos2 = Vector2.LerpUnclamped( sampledPositions3D[i].pos, sampledPositions3D[i+1].pos, alpha );
                        var angd = Mathf.LerpAngle( sampledPositions3D[i].ang, sampledPositions3D[i+1].ang, alpha );
                        //var angd = sampledPositions3D[i].ang;
                        return (pos2, angd);
                    }
                }
                var lastElem = sampledPositions3D.Last();
                return (lastElem.pos, lastElem.ang);
            }
        }

        public (Vector3 posWS, float angDeg) InterpolatedPositionWS( Vector2 rdOrigin )
        {
            var p  = InterpolatedPositionRD();
            var p2 = p.pos - rdOrigin;
            return (new Vector3(p2.x, 0, p2.y), p.ang);
        }

        public Vector3 ActualPositionWS(  Vector2 rdOrigin )
        {
            var p2 = PositionRD - rdOrigin;
            return new Vector3(p2.x, 0, p2.y);
        }

        public Vector3 PredictedPositionWS( Vector2 rdOrigin )
        {
            var p2 = predictedRD - rdOrigin;
            return new Vector3( p2.x, 0, p2.y );
        }
    }

    public class OVClient
    {
        volatile bool closing;
        string ip;
        int port;
        TcpClient tcp;
        Vector2 originRd;
        float catchRadius;
        Stopwatch stopwatch;
        long requestOvUpdateDelay = 300;
        Dictionary<string, OVVehicle> vehicles;
        Thread receiveThead;

        public long RequestDelay => requestOvUpdateDelay;
        public long ElapsedMilliseconds => stopwatch.ElapsedMilliseconds;
        public bool IsReady => (tcp!=null && tcp.Connected && receiveThead!=null && !closing);


        public List<OVVehicle> GetVehicles() // If catch radius is not too big, we are only dealing with a small amount of vehicles (100+ lists). Make a copy.
        {  
            lock (vehicles)
            {
                return vehicles.Select( kvp => kvp.Value ).ToList();
                //return vehicles.Where( kvp=> kvp.Value.route!=null).Select( kvp => kvp.Value ).ToList();
            }
        }
  
        public OVClient( Vector2 _originRD, string _ip="127.0.0.1", int _port=9999, float catchRadiusMtrs = 5000, long requestDelayMs = 1000 )
        {
            ip=_ip;
            port=_port;
            originRd=_originRD;
            catchRadius=catchRadiusMtrs;
            requestOvUpdateDelay = requestDelayMs;
            stopwatch = new Stopwatch();
            vehicles  = new Dictionary<string, OVVehicle>();
            stopwatch.Start();
        }

        public void Start()
        {
            if (receiveThead != null)
                throw new InvalidOperationException( "First stop, then start again." );
            closing = false;
            receiveThead = new Thread(() => {ReceiveDataAsync(); });
            receiveThead.Start();   
        }
        public void Stop()
        {
            closing = true;
            receiveThead?.Join( 1000 );
            receiveThead = null;
            tcp?.Close();
            tcp=null;
        }

        void RequestVehicleUpdate()
        {
            if (tcp == null)
                throw new InvalidOperationException( "Not connected." );
            SendMsg( "vehicles", "" );
        }

        void UpdateRouteForVehicle(OVVehicle vehicle)
        {
            if (tcp == null)
                throw new InvalidOperationException( "Not connected." );

            var msg = $"{vehicle.owner}|{vehicle.journeyNumb}|{vehicle.vehicleNumb}|{vehicle.linePrivateNumb}";
            SendMsg( "journey", msg );
        }

        void SendMsg( string id, string msg )
        {
            string payload = $"{id}|{msg}####";
          //  tcp.GetStream().Write( Encoding.UTF8.GetBytes( payload ) );
            tcp.GetStream().WriteAsync( Encoding.UTF8.GetBytes( payload ) ); // Seems to hang on closing.
        }

        void ReceiveDataAsync()
        {
            string bufferedData = "";
            bool wasConnected = false;
            long lastRequestTime = stopwatch.ElapsedMilliseconds;
            while (!closing)
            {
                try
                {
                    if (tcp == null)
                    {
                        tcp = new TcpClient();
                        //tcp.ConnectAsync( ip, port );
                        tcp.Connect( ip, port );
                    }

                    if (wasConnected && !tcp.Connected)
                    {
                        tcp.Close();
                        tcp = null;
                        wasConnected = false;
                        continue;
                    }

                    if (tcp.Connected && tcp.Available > 0)
                    {
                        wasConnected   = true;
                        int numToRead  = tcp.Available;
                        byte [] buffer = new byte[numToRead];
                        int actualRead = tcp.GetStream().Read( buffer, 0, numToRead );
                        bufferedData  += Encoding.UTF8.GetString( buffer, 0, actualRead );
                        while (true)
                        {
                            int splitIdx = bufferedData.IndexOf( "####" );
                            if (splitIdx >= 0)
                            {
                                var data = bufferedData.Substring( 0, splitIdx );
                                ProcessData( data );
                                bufferedData = bufferedData.Substring( splitIdx + 4, bufferedData.Length - (splitIdx+4) );
                            }
                            else break;
                        }
                    }

                    if ( tcp.Connected && stopwatch.ElapsedMilliseconds - lastRequestTime > requestOvUpdateDelay )
                    {
                        lastRequestTime = stopwatch.ElapsedMilliseconds;
                        RequestVehicleUpdate();
                        UnityEngine.Debug.Log( "Sent vehicle request" );
                    }

                    Thread.Sleep( 30 ); // Not optimal (non-blocking) but makes closing a lot cleaner.
                }
                catch (Exception e)// when (!(e is SocketException)) // Occurs when connection fails (e.g. server is not running).
                {
                    UnityEngine.Debug.LogWarning( e.Message );
                    tcp = null;
                }
            }
        }

        void ProcessData( string data )
        {
            var fields = data.Split( '|' );
            switch (fields[0])
            {
                case "vehicles":
                    ProcessVehiclesCSV( fields[1] );
                //    UnityEngine.Debug.Log( "Recv vehicle request" );
                    break;

                case "journey":
                    ProcessJourney( fields );
                    break;
            }
        }

        string MakeVehicleKey(string owner, string vehicleNumb)
        {
            return string.Join( ':', owner, vehicleNumb );
        }

        void ProcessVehiclesCSV( string csv )
        {
            var lines = csv.Split( '\n', StringSplitOptions.RemoveEmptyEntries );
            lock (vehicles)
            {
                foreach( var kvp in vehicles )
                {
                    kvp.Value.netUpdated=false;
                }

                for (int i = 1;i <lines.Length;i++)
                {
                    var values = lines[i].Trim('\r').Split( ',' );

                    if (!(bool.TryParse( values[(int)VehicleInfoType.MustTeleport], out bool teleport ) &&
                          float.TryParse( values[(int)VehicleInfoType.Rdx], out float rdX ) &&
                          float.TryParse( values[(int)VehicleInfoType.Rdy], out float rdY )))
                    {
                        continue;
                    }

                    float wsX = rdX - originRd.x;
                    float wsY = rdY - originRd.y;
                    var pos2d = new Vector2( wsX, wsY );

                    var owner = values[(int)VehicleInfoType.Owner];
                    var vNumb = values[(int)VehicleInfoType.VehicleNumber];
                    var key   = MakeVehicleKey(owner, vNumb);

                    // Outdated or outside interest radius.
                    if (pos2d.magnitude > catchRadius)
                    {
                        vehicles.Remove( key );
                        continue;
                    }

                    if (!vehicles.TryGetValue( key, out OVVehicle v ))
                    {
                        v = new OVVehicle();
                        v.key    = key;
                        v.client = this;
                        v.owner  = owner;
                        v.vehicleNumb = vNumb;
                        vehicles.Add( key, v );
                    }

                    v.netUpdated        = true;
                    v.lineDisplayNumb   = values[(int)VehicleInfoType.LineDisplayNumber];
                    v.linePrivateNumb   = values[(int)VehicleInfoType.LinePrivateNumber];

                    var prevJourneyNumb = v.journeyNumb;
                    v.journeyNumb       = values[(int)VehicleInfoType.JourneyNumber];

                    if (teleport)
                    {
                        v.CleanPredictedSamples();
                    }

                    if ( prevJourneyNumb != v.journeyNumb )
                    {
                        UpdateRouteForVehicle( v );
                        v.CleanPredictedSamples();
                    }

                    v.positionRD = new Vector2( rdX, rdY );
                    

                    float.TryParse( values[(int)VehicleInfoType.PredictedRdx], out v.predictedRD.x );
                    float.TryParse( values[(int)VehicleInfoType.PredictedRdy], out v.predictedRD.y );
                    float.TryParse( values[(int)VehicleInfoType.AngleDeg], out v.angleDeg );
                    float.TryParse( values[(int)VehicleInfoType.SpeedMtrps], out v.speedMtrps );

                    if ( v.predictedRD.x == 0 || v.predictedRD.y == 0 )
                    {
                        UnityEngine.Debug.LogWarning( "Invalid predicted position." );
                        continue;
                    }

                    v.AddPredictedPositionRD();
                }

                var pendingDeletes = new List<string>();
                foreach (var kvp in vehicles)
                {
                    if (!kvp.Value.netUpdated)
                        pendingDeletes.Add( kvp.Key );
                }
                pendingDeletes.ForEach( v => vehicles.Remove( v ) );
            }
        }

        void ProcessJourney( string[] fields ) 
        {
            OVRoute route = new OVRoute();
            route.stops = new List<(string name, Vector2 position, string dest)>();
            route.routePoints = new List<Vector2>();
            string owner = fields[1];
            string vehicleNumb = fields[2];
            route.lineName = fields[3];
            route.linePrivateNumb = fields[4];
            route.transportType = fields[5].ToLowerInvariant();
            route.lineDisplayNumb  = fields[6];
            int numStops = int.Parse( fields[7] );
            string prevDest = string.Empty;
            int of = 8;
            for (int i = 0;i < numStops;i++)
            {
                var name = fields[of+i*4];
                var x = float.Parse(fields[of+i*4+1]);
                var y = float.Parse(fields[of+i*4+2]);
                var dest = fields[of+i*4+3];
                if (dest != string.Empty)
                {
                    prevDest = dest;
                }
                route.stops.Add( (name, new Vector2(x,y), prevDest) );
            }
            of = of+numStops*4;
            int numRoutePoints = int.Parse( fields[of] );
            for (int i = 0;i < numRoutePoints;i++)
            {
                var x = float.Parse( fields[1+of+i*2+0] );
                var y = float.Parse( fields[1+of+i*2+1] );
                route.routePoints.Add( new Vector2(x, y) );
            }
            string key = MakeVehicleKey( owner, vehicleNumb );
            if(vehicles.TryGetValue(key, out var vehicle ))
            {
                vehicle.route = route;
                vehicle.routeRevision++;
            }
        }
    }
}
