using System.Linq;
using UnityEngine;

namespace Wander
{
    [RequireComponent(typeof(OVManager))]
    [RequireComponent(typeof(LineRenderer))]
    public class OVSelector : MonoBehaviour
    {
        [Header("References")]
        public Camera cam;

        [Header("LineVariables")]
        public float minWidth = 0.3f;
        public float maxSize =  5.0f;
        public float maxHeightCam = 300;
        public GameObject activeLineObject;

        OVManager ovManager;
        LineRenderer lineRenderer;
        GameObject prevActiveLineObject;

        private void Awake()
        {
            if (cam == null)
            {
                cam = Camera.main;
                if (cam == null)
                {
                    Debug.LogError( "Cam cannot be null, selecting objects wil not work. Behaviour will be turned off (Disabled)." );
                    enabled = false;
                }
            }
            
            ovManager    = GetComponent<OVManager>();
            lineRenderer = GetComponent<LineRenderer>();
            activeLineObject = null; // Avoid serialization from disk but keep public so can be adjusted in PIE.
        }

        void Start()
        {
            if ( LayerMask.GetMask("OV")==0 )
            {
                Debug.LogError( "Layer 'OV' not found. Please add to Layermanager in unity project on position 20. Otherwise selecting OV objects will not work on Play." );
            }
        }

        private void Update()
        {
            SelectOVObjectFromMouse();
            VisualizeLineOfActiveObject();
        }


        void SelectOVObjectFromMouse()
        {
            if ( !Input.GetMouseButtonDown(0) )
            {
                return;
            }

            var mousePos = Input.mousePosition;
            Ray ray = cam.ScreenPointToRay( mousePos );
            var msk = LayerMask.GetMask("OV");
            if (ray.Raycast(5000, msk, out var hit))
            {
                var behaviour = hit.collider.gameObject.GetComponent<OVVehicleBehaviour>();
                if (behaviour == null)
                {
                    if (hit.collider.transform.parent != null) // Collider might be on child object, try parent.
                    {
                        behaviour = hit.collider.transform.parent.GetComponent<OVVehicleBehaviour>();
                    }
                }
                if (behaviour != null)
                {
                    if (activeLineObject!=null)
                        activeLineObject.GetComponent<OVVehicleBehaviour>().enabled=false;
                    activeLineObject = behaviour.gameObject;
                    behaviour.cam = cam;
                    behaviour.enabled = true;
                }
            }
        }

        void VisualizeLineOfActiveObject()
        {
            Debug.Assert( cam != null );

            float lineWidth = Mathf.Lerp(minWidth, maxSize, cam.transform.position.y/maxHeightCam);
            lineRenderer.startWidth = lineRenderer.endWidth = lineWidth;

            if (activeLineObject == prevActiveLineObject)
                return;

            prevActiveLineObject = activeLineObject;
            if (activeLineObject == null)
                return;

            var behaviour = activeLineObject.GetComponent<OVVehicleBehaviour>();
            if (behaviour==null)
                return;

            var route = behaviour.ovVehicle.Route;
            if (route == null)
                return;

            lineRenderer.positionCount = behaviour.ovVehicle.Route.RoutePoints.Count;
            lineRenderer.SetPositions( behaviour.ovVehicle.Route.RoutePoints.Select( rp =>
            {
                var ws = rp-ovManager.originRD;
                float y = 0;
                if (new Vector3( ws.x, 0, ws.y ).RaycastDown( 200, 500, LayerMask.GetMask( "Default" ), out var hit ))
                    y = hit.point.y+0.1f;
                return new Vector3( ws.x, y, ws.y );
            } ).ToArray() );
        }
    }
}